/**
 * Created by Alfredo on 5/3/2015.
 * Last modified 5/3/2016
 */

(function ($) {

    var global = {};

    $.fn.objectize = function () {

    };

    // Plugin definition.
    $.fn.objectize = function (options) {

        global = {};
        // Extend our default options with those provided.
        // Note that the first argument to extend is an empty
        // object � this is to keep from overriding our "defaults" object.
        var opts = $.extend({}, $.fn.objectize.defaults, options);

        // Our plugin implementation code goes here.
        // Iterate and reformat each matched element.
        this.each(function () {

            global = {};
            var container = $(this);
            var value = undefined;

            container.find("input,select,textarea,span").each(function () {
                var element = $(this);

                switch (element.tagName()) {
                    case'radio':
                        if (element.is(":checked")) {
                            value = element.prop(opts.value);

                            if(value === undefined)
                                value = element.attr(opts.value);

                            if (element.prop(opts.identifier) !== undefined) {
                                $.extend(true, global, createObject(element.prop(opts.identifier).split(opts.separator), value));
                            } else if (element.attr(opts.identifier) !== undefined) {
                                $.extend(true, global, createObject(element.attr(opts.identifier).split(opts.separator), value));
                            }
                            //console.log('I am a ' + element.tagName() + ' and my value is ' + element.prop(opts.value) );
                        }
                        //'[type="radio"]:checked'
                        break;
                    case 'checkbox':
                        if (element.is(":checked")) {
                            value = element.is(":checked");
                            if (element.prop(opts.identifier) !== undefined) {
                                $.extend(true, global, createObject(element.prop(opts.identifier).split(opts.separator), value));
                            } else if (element.attr(opts.identifier) !== undefined) {
                                $.extend(true, global, createObject(element.attr(opts.identifier).split(opts.separator), value));
                            }
                            //console.log('I am a ' + element.tagName() + ' and my value is ' + element.prop(opts.value) );
                        }
                        //'[type="checkbox"]:checked'
                        break;
                    case 'select':
                    case 'option':
                        element.find("option:selected").each(function () {
                            value = $(this).prop(opts.value);

                            if(value === undefined)
                                value = $(this).attr(opts.value);

                            if ($(this).parent().prop(opts.identifier) !== undefined) {
                                $.extend(true, global, createObject($(this).parent().prop(opts.identifier).split(opts.separator), value));
                            } else if ($(this).parent().attr(opts.identifier) !== undefined) {
                                $.extend(true, global, createObject($(this).parent().attr(opts.identifier).split(opts.separator), value));
                            }

                            if ($(this).parent().parent().prop(opts.identifier) !== undefined) {
                                $.extend(true, global, createObject($(this).parent().parent().prop(opts.identifier).split(opts.separator), value));
                            } else if ($(this).parent().parent().attr(opts.identifier) !== undefined) {
                                $.extend(true, global, createObject($(this).parent().parent().attr(opts.identifier).split(opts.separator), value));
                            }
                        });
                        //"select option:selected"
                        break;
                    case 'textarea':
                    default :
                        value = element.prop(opts.value);

                        if(value === undefined)
                            value = element.attr(opts.value);

                        if (element.prop(opts.identifier) !== undefined) {
                            $.extend(true, global, createObject(element.prop(opts.identifier).split(opts.separator), value));
                        } else if (element.attr(opts.identifier) !== undefined) {
                            $.extend(true, global, createObject(element.attr(opts.identifier).split(opts.separator), value));
                        }
                    //console.log('I am a ' + element.tagName() + ' and my value is ' + element.prop(opts.value) );
                }
            });

            // ...
            //var markup = elem.html();
            // Call our format function.
            //markup = $.fn.hilight.format( markup );

            //elem.html( markup );

        });
        return jQuery.isEmptyObject(global) ? undefined : global;
    };


    // Plugin defaults � added as a property on our plugin function.
    $.fn.objectize.defaults = {
        container: ".objectize input",
        separator: ".",
        identifier: "name",
        value: "value"
    };

    $.fn.tagName = function () {
        //this.prop("type").toLocaleLowerCase() === "input"
        return this.is("input") ? this.prop("type").toLowerCase() : this.prop("tagName").toLowerCase();
    };

    String.prototype.trim = function () {
        //console.log('override');
        return this.replace(/^[ ]*/, '').replace(/[ ]*$/, '');
    };

    function createObject(array, value) {
        if (array.length === 0) {
            return (value !== undefined && value.toString().trim() !== undefined) ? value : null;
        } else {
            var obj = {};
            obj[array[0]] = createObject(array.splice(1), value);
            return obj;
        }
    }

    $.identity = function (str, context, value) {
        if (str === undefined)
            return { error: { message: "identifier is undefined"}};
        if (context === undefined)
            return { error: { message: "context is undefined"}};

        var split = str.toString().split($.fn.objectize.defaults.separator);
        for (var i = 0; i < split.length; i++) {
            context = context[split[i]];
        }
        return (value !== undefined && value == true) ? context : (context !== undefined);
    };

}(jQuery));
