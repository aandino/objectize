# Objectize #


## What objetize does? ##

* Is very simple create a JSON form a HTML form
* V 1.0
* [Learn Markdown](https://bitbucket.org/aandino/objectize)

## Have dependencies? ##
Yes, JQuery

## How do I get set up? ##

### Parameters ###
* container(form) = container of inputs
* separator(default: .) = name identifier separeted at. To build nested object with the naming convention
* identifier(default attribute: name) = html attribute uses as identifier
* value(default attribute: value) = html attribute thats contains the value of the input.

### Uses ###
* Use as Default: $("container").objectize();
* Use as Custom:  $("container").objectize({container: ".objectize", separator: ".", identifier: "name", value: "value"});

**Develop by: ** by aandino